<?php
/**
 * @file
 *   The Facebook post template. This template can be customized to output a Facebook post in anyway the user wants.
 * 
 *  @param $data
 *    The actual facebook post data. An array that looks similar to the following
 *      [id] => 19046019942_438887859942
        [from] => Array
            (
                [name] => Greater Androscoggin Humane Society
                [category] => Non-profit organization
                [id] => 19046019942
            )

        [message] => Everyone will be considered Irish this month, because the Irish are lucky!!  Now to the end of March all our spay/neuter programs will be free.  We have a sponsor paying all the $10 co-pays for the month..  Spread the word..  Free Pit Bull and Pit Bull mix spay / neuters; Free Community Kitten spay/neuters; Free cat spays/neuters for families in need of financial assistance...  Here is a link to our spay/neuter programs http://www.gahumane.org/financial-assistance
        [picture] => https://fbcdn-photos-a.akamaihd.net/hphotos-ak-ash4/427680_438887839942_19046019942_1535319_1635146694_s.jpg
        [link] => http://www.facebook.com/photo.php?fbid=438887839942&set=a.40920364942.16958.19046019942&type=1
        [icon] => https://s-static.ak.facebook.com/rsrc.php/v1/yz/r/StEh3RhPvjk.gif
        [actions] => Array
            (
                [0] => Array
                    (
                        [name] => Comment
                        [link] => http://www.facebook.com/19046019942/posts/438887859942
                    )

                [1] => Array
                    (
                        [name] => Like
                        [link] => http://www.facebook.com/19046019942/posts/438887859942
                    )

            )

        [privacy] => Array
            (
                [description] => Public
                [value] => EVERYONE
            )

        [type] => photo
        [object_id] => 438887839942
        [created_time] => 2012-03-02T17:29:29+0000
        [updated_time] => 2012-03-02T17:52:00+0000
        [shares] => Array
            (
                [count] => 12
            )

        [likes] => Array
            (
                [data] => Array
                    (
                        [0] => Array
                            (
                                [name] => Lee Ann Black
                                [id] => 1227521787
                            )

                    )

                [count] => 11
            )

        [comments] => Array
            (
                [data] => Array
                    (
                        [0] => Array
                            (
                                [id] => 19046019942_438887859942_1078322
                                [from] => Array
                                    (
                                        [name] => Greater Androscoggin Humane Society
                                        [category] => Non-profit organization
                                        [id] => 19046019942
                                    )

                                [message] => This includes your free roaming cats that you care for.
                                [created_time] => 2012-03-02T17:52:00+0000
                            )

                    )

                [count] => 1
            )

        [is_published] => 1
    )
 * 
 * @param $fb
 *   The Facebook api object, useful for pulling additional data into the post.
 */
$p_data = $data->data;
?>
<div class="fb-post fb-post-<?php print $p_data['type'];?>">
  <div class="fb-post-pic"><?php print l($pic, $profile_link, array('html' => TRUE)); ?></div>
  <div class="fb-post-right">
    <div class="fb-post-from"><?php print l($p_data['from']['name'], $profile_link);?></div>
    <div class="fb-post-message"><?php print $data->message; ?></div>
    <div class="fb-post-meta">
      <?php if (!empty($p_data['icon'])): ?>
      <span class="fb-post-meta-icon"><?php print theme('image', array('path' => $p_data['icon'])); ?></span>
      <?php endif;?>
      <span class="fb-post-meta-actions"><?php 
        $sep = '';
        foreach ($p_data['actions'] as $action) {
          print $sep . l($action['name'], $action['link']);
          $sep = " · ";
        }  
      ?></span>
      <?php if (!empty($p_data['likes']['count'])): ?>
      <span class="fb-post-meta-likes"></span>
      <?php endif;?>
      <?php if (!empty($p_data['comments']['count'])): ?>
      <span class="fb-post-meta-comments"></span>
      <?php endif;?>
      <?php if (!empty($p_data['shares']['count'])): ?>
      <span class="fb-post-meta-shares"></span>
      <?php endif;?>
      <span class="fb-post-meta-time"> · Posted <?php print $timestamp; ?> ago</span>
    </div>
  </div>
</div>
