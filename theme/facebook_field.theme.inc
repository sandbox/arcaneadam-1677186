<?php
/*
 * @file
 *   Contains theme functions and other theme related material
 */

function theme_fb_profile_image($vars) {
  $id = $vars['id'];
  $name = $vars['name'];
  $path = "https://graph.facebook.com/{$id}/picture";
  $img_vars = array(
    'path' => $path,
    'alt' => $name,
    'title' => $name,
    'width' => 50,
    'height' => 50,
    'attributes' => array(
      'class' => array(
        'facebook_field_profile_image'
      ),
    ),
  );
  return theme('image', $img_vars);
}

function facebook_field_preprocess_fb_stream(&$vars) {
  
}



function facebook_field_preprocess_fb_post(&$vars) {
  $profiles = &drupal_static(__FUNCTION__);
  $vars['pic'] = theme('fb_profile_image', $vars['data']->data['from']);
  $id = $vars['data']->data['from']['id'];
  if (empty($profiles[$id])) {
    $profiles[$id] = $vars['fb']->api("/{$id}");
  }
  $vars['profile_link'] = $profiles[$id]['link'];
  if (isset($vars['keyword'])) {
    $vars['data']->message = preg_replace('/(' . $vars['keyword'] . ')/iu', '<strong>\0</strong>', $vars['data']->message);
  }
  $time = strtotime($vars['data']->created);
  $time_diff = REQUEST_TIME - $time;
  $vars['timestamp'] = format_interval($time_diff, 2);
  $vars['data']->message = check_markup($vars['data']->message, 'filtered_html');
  if (!isset($vars['no_trim'])) {
    $vars['data']->original_message = $vars['data']->message;
    $vars['data']->message = views_trim_text(array('max_length' => 300, 'word_boundary' => TRUE, 'ellipsis' => TRUE, 'html' => TRUE), $vars['data']->message);
  }
}

function facebook_field_preprocess_fb_post__photo(&$vars) {
  facebook_field_preprocess_fb_post(&$vars);
}

function facebook_field_preprocess_fb_post__video(&$vars) {
  facebook_field_preprocess_fb_post(&$vars);
  if ($vars['data']->data['type'] == 'video') {
    try {
      $params = array('access_token' => $vars['access_token']);
      $vars['video'] = $vars['fb']->api('/' . $vars['data']->data['object_id'], $params);
    }
    catch (FacebookApiException $e) {
    }
  }
}