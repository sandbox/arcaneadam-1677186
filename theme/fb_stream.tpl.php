<?php
/**
 * @file
 *   Facebook Stream theme file
 */
?>
<div class="fb-stream">
  <?php
    foreach($data as $post) {
      $post->data = unserialize($post->data);
      $vars = array();
      $vars['fb'] = $fb;
      $vars['data'] = $post;
      if (isset($keyword)) {
        $vars['keyword'] = $keyword;
      }
      $vars['access_token'] = $access_token;
      if (isset($no_trim)) {
        $vars['no_trim'] = TRUE;
      }
      print theme(array('fb_post__' . $post->data['type'], 'fb_post'), $vars);
    }
  ?>
  <?php if ($more_link):?>
    <div class="more-link">
      <?php print $more_link; ?>
    </div>
  <?php endif;?>
</div>
