<?php
/**
 * @file FacebookField Class
 */
// There are times on the ajax form calls where this file gets called without the Facebook class
// being loaded first due to it not being in the registry. We simply ensure that it is loaded with the script below.
// Find Facebook's PHP SDK.  Use libraries API if enabled.
$fb_lib_path = function_exists('libraries_get_path') ? libraries_get_path('facebook-php-sdk') : 'sites/all/libraries/facebook-php-sdk';
$fb_platform = variable_get(FB_VAR_API_FILE, $fb_lib_path . '/src/facebook.php');

  try {
    if (!class_exists('Facebook') && !include($fb_platform)) {
      $message = t('Failed to find the Facebook client libraries at %filename.  Read the !readme and follow the instructions carefully.', array(
                     '!drupal_for_facebook' => l(t('Drupal for Facebook'), 'http://drupal.org/project/fb'),
                     // This link should work with clean URLs disabled.
                     '!readme' => '<a href='. base_path() . '/' . drupal_get_path('module', 'fb') . '/README.txt>README.txt</a>',
                     '%filename' => $filename,
                   ));
      drupal_set_message($message, 'error');
      watchdog('fb', $message);
      return NULL;
    }
  
    if (Facebook::VERSION < "3") {
      $message = 'This version of modules/fb is compatible with Facebook PHP SDK version 3.x.y, but %version was found (%fb_platform).';
      $args = array('%fb_platform' => $fb_platform, '%version' => Facebook::VERSION);
      if (user_access('access administration pages')) {
        drupal_set_message(t($message, $args));
      }
      watchdog('fb', $message, $args, WATCHDOG_ERROR);
      return NULL;
    }
  }
  catch (Exception $e) {
    fb_log_exception($e, t('FacebookField failed to be able to load the Facebook class.'));
  }
/**
 * Extends the Facebook class with the intent of using
 * caching to store requests.
 */
class FacebookField extends Facebook {
  protected $cache = 0;
  /**
   * Identical to the parent constructor, except that
   * we start a PHP session to store the user ID and
   * access token if during the course of execution
   * we discover them.
   *
   * @param Array $config the application configuration.
   * @see BaseFacebook::__construct in facebook.php
   */
  public function __construct($config) {
    if (!empty($config['cache'])) {
      $this->cache = $config['cache'];
    }
    parent::__construct($config);
  }
  
  /**
   * Make an API call after first checking the current
   * memory, then the database cache.
   *
   * @return mixed The decoded response
   */
  public function api(/* polymorphic */) {
    $args = func_get_args();
    if (!is_array($args[0])) {
      $static = &drupal_static(__CLASS__.'::'.__METHOD__, array());
      $api = $args[0];
      //We need to append the access token to the cache key because
      //it allows paths like /me to be stored in the system
      $key = $api . '-' . $this->accessToken;
      //Lets now set our cache time or leave it blank
      $cache = !$this->cache ? NULL : $this->cache + REQUEST_TIME;
      //First we check if the same call has been made during this page load
      if (isset($static[$key])) {
        return $static[$key];
      }
      //Next we check if this call is stored in the Databse
      else {
        $resp = cache_get($key, 'cache_fb_field');
        if ($resp) {
          return $resp->data;
        }
      }
      if (PHP_VERSION < 5.3) {
        $resp = call_user_func_array(array('parent', 'api'), $args);
      }
      else {
        $resp = call_user_func_array('parent::api', $args);
      }
      $static[$key] = $resp;
      if ($cache) {
        cache_set($key, $resp, 'cache_fb_field', $cache);
      }
      return $resp;
    }
    if (PHP_VERSION < 5.3) {
      return call_user_func_array(array('parent', 'api'), $args);
    }
    else {
      return call_user_func_array('parent::api', $args);
    }
  }
}
